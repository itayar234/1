#include "queue.h"
#include <iostream>

#define EMPTY_ARR -1

using std::cout;
using std::cin;

unsigned int end = 0;
unsigned int currSize = 0;



/*
Creates the queue
Input: 
	   q:    the queue that needs to be created
       size: the size of the queue to be created
Output: None
*/
void initQueue(queue* q, unsigned int size)
{
	q = new queue[size];
	end = 0;
	currSize = size;
	//cout << (q + start)->num;
}


/*
Cleans the queue
Input: the queue to be restarted
Output: none
*/
void cleanQueue(queue* q)
{
	
	delete &q;
}


/*
Adds a new item to the queue
Input: 
	   q:        the queue to add a new item to
       newValue: the value of the new item
Output: none 
*/
void enqueue(queue* q, unsigned int newValue)
{
	if (!newValue)
	{
		cout << "The item's value can't be 0!";
	}
	else if (end == currSize)
	{
		cout << "The array is full!";
	}
	else
	{
		(q + end)->num = newValue;
		end++;
	}
}


/*
Removes the first item from the list
Input: the queue to remove the item from
Output: the first item's value, or -1 if the array is empty
*/
int dequeue(queue* q)
{
	if (q->num)
	{
		unsigned int i = 0;
		for (i = 0; i < end - 1; i++)
		{
			*q = *(q + 1);
		}
		(q + end - 1)->num = 0;
		end--;
		if (q->num)
		{
			return q->num;
		}
		return EMPTY_ARR;
	}
	return EMPTY_ARR;
}